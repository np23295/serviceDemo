package com.nimit.serviceandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    AppCompatButton mStart, mStop;
    private String TAG =getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mStart = (AppCompatButton) findViewById(R.id.startservice_button);
        mStop = (AppCompatButton) findViewById(R.id.stopservice_button);
        mStart.setOnClickListener(this);
        mStop.setOnClickListener(this);
    }

    public void startService() {
        startService(new Intent(this, DemoService.class));
    }

    // Method to stop the service
    public void stopService() {
        stopService(new Intent(getBaseContext(), DemoService.class));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.startservice_button:
                startService();
                break;
            case R.id.stopservice_button:
                stopService();
                break;
            default:
                break;
        }
    }
}
